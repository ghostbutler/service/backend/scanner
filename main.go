package main

import (
	"./service"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/signal"
	"os"
	"time"
)

const(
	DefaultConfigurationPath = "conf.json"
)

func main( ) {
	// is running?
	isRunning := true

	// configuration file path
	configurationFilePath := DefaultConfigurationPath
	if len( os.Args ) > 1 {
		configurationFilePath = os.Args[ 1 ]
	}

	// load configuration
	if configuration, err := service.BuildConfiguration( configurationFilePath ); err == nil {
		// build server
		service.BuildService( common.GhostService[ common.ServiceScanner ].DefaultPort,
			configuration )

		// listen for signal
		signal.Listen( &isRunning )

		// wait for server
		for isRunning {
			time.Sleep( 16 * time.Millisecond )
		}

		// exit
		os.Exit( 0 )
	} else {
		fmt.Println( err )
		os.Exit( 1 )
	}

}
