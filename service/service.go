package service

import (
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/keystore"
)

const (
	VersionMajor = 1
	VersionMinor = 0
)

type Service struct {
	// configuration
	configuration *Configuration

	// service
	service *common.Service

	// scanner instance
	scanner *Scanner

	// key manager
	keyManager *keystore.KeyManager
}

// build server
func BuildService(listeningPort int,
	configuration *Configuration) *Service {
	// allocate
	service := &Service{
		configuration: configuration,
		keyManager: keystore.BuildKeyManager(configuration.SecurityManager.Hostname,
			configuration.SecurityManager.Username,
			configuration.SecurityManager.Password,
			common.GhostService[common.ServiceScanner].Name),
	}

	// create service
	service.service = common.BuildService(listeningPort,
		common.ServiceScanner,
		EndpointList,
		VersionMajor,
		VersionMinor,
		nil,
		nil,
		configuration.SecurityManager.Hostname,
		service)

	// build scanner
	service.scanner = BuildScanner(service.keyManager)

	// done
	return service
}
