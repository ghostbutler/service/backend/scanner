package service

import "time"

const (
	// thread count for detection (for each interface)
	ScannerThreadCount = 50

	// delay before detection rule removal
	DetectionRuleRemovalDelay = time.Minute
)

var(
	// interface filter, these interfaces will be ignored (regex)
	InterfaceFilter = [ ]string{
		"lo",
		"docker*",
	}
)

// this is the definition of the method to detect a service
// see the readme for more details on how to write json for this
type DetectionRule struct {
	// rule name
	Name string `json:"name"`

	// the port to do request on
	Port int `json:"port"`

	// the path to request
	Path string `json:"path"`

	// http method
	Method string `json:"method"`

	// does request use HTTPS?
	IsHTTPS bool `json:"isHTTPS"`

	// data to be sent (can be empty)
	Data string `json:"data"`

	// possible answers
	PossibleAnswer [ ]struct {
		// the response code to consider
		ResponseCode int `json:"responseCode"`

		// header analysis (if more then one, all headers conditions must be true)
		IsMustAnalyzeHeader bool `json:"isMustAnalyzeHeader"`
		HeaderAnalysis [ ]struct {
			// the header name
			Name string `json:"name"`

			// the expected value (base64 format to do not have problem with special character such as ")
			ExpectedValue string `json:"expectedValue"`

			// must be equal? if not it must only contains the ExpectedValue
			IsMustBeEqual bool `json:"isMustBeEqual"`
		} `json:"validHeader"`

		// content analysis (if more then one, at least one one the content expectation has to be true)
		IsMustAnalyzeContent bool `json:"isMustAnalyzeContent"`
		ContentAnalysis [ ]struct{
			// the expected value (base64 format to do not have problem with special character such as ")
			ExpectedValue string `json:"expectedValue"`

			// must be equal? if not it must only contains the ExpectedValue
			IsMustBeEqual bool `json:"isMustBeEqual"`
		} `json:"validContent"`
	} `json:"possibleAnswer"`
}

// controller details
type Controller struct {
	// controller IP
	IP string `json:"ip"`

	// controller listening port
	Port int `json:"port"`

	// controller name
	Name string `json:"name"`

	// URL path where to send the detected IP
	Path string `json:"path"`
}

// this is the structure saving the controller to contact when a service is identified,
// and how a device is identified
type DetectionOrder struct {
	// detection rule
	Rule DetectionRule `json:"rule"`

	// controller data
	Controller Controller `json:"controller"`

	// last update of this rule
	// when a rule is not updated for a too long time, it disappears from detection list
	lastUpdate time.Time

	// is permanent?
	IsPermanent bool `json:"isPermanent"`
}

// a network interface with its associated address
type NIC struct {
	// name
	Name string

	// ip address
	Address int

	// mask
	Mask int
}
