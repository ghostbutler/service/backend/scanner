package service

import (
	"encoding/json"
	"io/ioutil"
)

type Configuration struct {
	SecurityManager struct {
		Hostname string `json:"hostname"`

		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"securityManager"`
}

// build configuration
func BuildConfiguration( configurationFilePath string ) ( *Configuration, error ) {
	// output
	output := &Configuration{ }

	// read file
	if content, err := ioutil.ReadFile( configurationFilePath ); err == nil {
		if err = json.Unmarshal( content,
			output ); err == nil {
			return output, nil
		} else {
			return nil, err
		}
	} else {
		return nil, err
	}
}
