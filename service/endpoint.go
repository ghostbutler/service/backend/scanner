package service

import (
	"encoding/json"
	"gitlab.com/ghostbutler/tool/service"
	"gitlab.com/ghostbutler/tool/service/keystore"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	APIServiceV1ScannerRegister = common.APIServiceType(iota + common.APIServiceBuiltInLast)
)

var EndpointList = map[common.APIServiceType]*common.APIEndpoint{
	APIServiceV1ScannerRegister: {
		Path:                    []string{"api", "v1", "scanner", "register"},
		Method:                  "POST",
		Description:             "Register a new component detection rule to the scanner",
		Callback:                HandleNewComponentRegistration,
		IsMustProvideOneTimeKey: false,
	},
}

// handle new component registration
func HandleNewComponentRegistration(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// cast service
	service := handle.(*Service)

	// check one time key
	if publicKey, err := keystore.CheckOneTimeKey(request,
		service.configuration.SecurityManager.Hostname); err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte(common.BadRequestMessage))
		return http.StatusBadRequest
	} else {
		// device detection rule
		rule := &DetectionOrder{
			Controller: Controller{
				IP:   publicKey.ServiceIP,
				Name: publicKey.ServiceName,
			},
			lastUpdate: time.Now(),
		}

		// parse request body
		body, _ := ioutil.ReadAll(request.Body)
		if err := json.Unmarshal(body,
			rule); err != nil {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte(common.BadRequestMessage))
			return http.StatusBadRequest
		}

		// check rule
		if rule.Rule.Port <= 0 ||
			len(rule.Rule.Path) <= 0 ||
			len(rule.Rule.Method) <= 0 ||
			len(rule.Rule.PossibleAnswer) <= 0 {
			rw.WriteHeader(http.StatusBadRequest)
			_, _ = rw.Write([]byte(common.BadRequestMessage))
			return http.StatusBadRequest
		}

		// add rule to rules list
		service.scanner.addRule(rule)
	}
	return http.StatusOK
}
