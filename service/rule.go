package service

import (
	"bytes"
	"encoding/base64"
	"gitlab.com/ghostbutler/tool/service"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

var DefaultRuleList = []string{
	// other scanner detection
	`{"isPermanent":true,"rule":{"port":16561,"path":"/api/v1/status","method":"GET","isHTTPS":true,"possibleAnswer":[{"responseCode":200,"isMustAnalyzeContent":true,"validContent":[{"expectedValue":"InR5cGUiOiJnaG9zdGJ1dGxlciIsIm5hbWUiOiJnaG9zdGJ1dGxlci5zY2FubmVyIg==","isMustBeEqual":true}],"isMustAnalyzeHeader":true,"validHeader":[{"name":"X-Ghost-Butler","expectedValue":"c3RhdHVz","isMustBeEqual":false}]}]},"controller":{"ip":"127.0.0.1","port":16561,"name":"scanner controller","path":"/api/v1/controller/add"}}`,
}

// build request for test
func buildTestRequest(ip string,
	rule *DetectionRule) *http.Request {
	// data buffer
	var data io.Reader = nil
	if len(rule.Data) > 0 {
		data = bytes.NewBuffer([]byte(rule.Data))
	}

	// build url
	var url string
	if rule.IsHTTPS {
		url = "https://"
	} else {
		url = "http://"
	}
	url += ip
	if rule.IsHTTPS &&
		rule.Port != 443 {
		url += ":" +
			strconv.Itoa(rule.Port)
	} else if !rule.IsHTTPS &&
		rule.Port != 80 {
		url += ":" +
			strconv.Itoa(rule.Port)
	}
	if len(rule.Path) > 0 {
		if rule.Path[0] == '/' {
			url += rule.Path
		} else {
			url += "/" +
				rule.Path
		}
	} else {
		url += "/"
	}

	// build request
	request, _ := http.NewRequest(strings.ToUpper(rule.Method),
		url,
		data)

	// built request
	return request
}

// check if a rule is accepted
func (scanner *Scanner) checkRule(rule *DetectionRule,
	response *http.Response,
	ip string) bool {
	// extract response body
	body, _ := ioutil.ReadAll(response.Body)

	// iterate possible answer for detection
	for _, possibleAnswer := range rule.PossibleAnswer {
		// is answer code correct?
		if possibleAnswer.ResponseCode == response.StatusCode {
			// we start guessing rule is OK
			isRuleOk := true

			// do we analyze content?
			if possibleAnswer.IsMustAnalyzeContent {
				// is the body containing something?
				if len(body) > 0 {
					// we look for at least one correct content, so we start supposing content is not OK
					// and when we'll find one which is correct, we'll break and continue
					isContentOk := false

					// iterate possible content rules
					for _, contentRule := range possibleAnswer.ContentAnalysis {
						// decode rule content
						if value, err := base64.StdEncoding.DecodeString(contentRule.ExpectedValue); err == nil {
							// is it supposed to be equal?
							if contentRule.IsMustBeEqual {
								if string(value) == string(body) {
									isContentOk = true
									break
								}
								// is it suppose to contain the expected value?
							} else {
								if strings.Contains(string(body),
									string(value)) {
									isContentOk = true
									break
								}
							}
							// couldn't decode rule content...
						} else {
							break
						}
					}

					// bad content, the rule is not valid
					if !isContentOk {
						isRuleOk = false
					}
					// body was empty, unable to analyze content
				} else {
					isRuleOk = false
				}
			}

			// if the previous check is passed, and if we're suppose to analyze header
			if isRuleOk &&
				possibleAnswer.IsMustAnalyzeHeader {
				// as we are supposed to validate every header rule, we start supposing check is OK
				isHeaderOk := true

				// iterate header rules
				for _, headerRule := range possibleAnswer.HeaderAnalysis {
					// decode header expected value
					if expectedValue, err := base64.StdEncoding.DecodeString(headerRule.ExpectedValue); err == nil {
						// get the header value
						if value := response.Header.Get(headerRule.Name); len(value) > 0 {
							// is value must be equal?
							if headerRule.IsMustBeEqual {
								if string(expectedValue) != value {
									isHeaderOk = false
									break
								}
								// is value must contains expected value?
							} else {
								if !strings.Contains(value,
									string(expectedValue)) {
									isHeaderOk = false
									break
								}
							}
							// header value was empty
						} else {
							isHeaderOk = false
							break
						}
						// unable to decode rule expected value
					} else {
						isHeaderOk = false
						break
					}
				}

				// did we failed one header checks?
				if !isHeaderOk {
					isRuleOk = false
				}
			}

			// the rule is valid
			if isRuleOk {
				return true
			}
		}
	}
	return false
}

// test rule on ip, return true if rule is validated, false else
func (scanner *Scanner) testRuleOnIP(ip string,
	rule *DetectionRule) bool {
	// build request
	request := buildTestRequest(ip,
		rule)

	// do request
	request.Close = true
	if response, err := scanner.httpClient.Do(request); err == nil {
		defer common.Close(response.Body)
		return scanner.checkRule(rule,
			response,
			ip)
	} else {
		return false
	}
}
